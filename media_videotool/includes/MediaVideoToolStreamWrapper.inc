<?php

/**
 *  @file media_videotool/includes/MediaVideoToolStreamWrapper.inc
 *
 *  Create a VideoTool Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $videotool = new MediaVideoToolStreamWrapper('videotool://v/[video-code]');
 */
class MediaVideoToolStreamWrapper extends MediaReadOnlyStreamWrapper {

  // Overrides $base_url defined in MediaReadOnlyStreamWrapper.
  protected $base_url = 'http://media.videotool.dk/';

  /**
   * Returns a url in the format "http://media.videotool.dk/?vn=qsPQN4MiTeE".
   *
   * Overrides interpolateUrl() defined in MediaReadOnlyStreamWrapper.
   * This is an exact copy of the function in MediaReadOnlyStreamWrapper,
   * here in case that example is redefined or removed.
   */
  function interpolateUrl() {
$myFile = "/mounted-storage/home112c/sub002/sc49404-CVYU/avm/log_file.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
fwrite($fh, "interpolateUrl called\n");
fclose($fh);	

    if ($parameters = $this->get_parameters()) {

$myFile = "/mounted-storage/home112c/sub002/sc49404-CVYU/avm/log_file.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
fwrite($fh, "interpolateUrl ".print_r($parameters, true)."\n");
fclose($fh);


      return $this->base_url . '?' . http_build_query($parameters);
    }
  }

  static function getMimeType($uri, $mapping = NULL) {

$myFile = "/mounted-storage/home112c/sub002/sc49404-CVYU/avm/log_file.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
fwrite($fh, "getMimeType called\n");
fclose($fh);	

    return 'video/videotool';
  }

  function getTarget($f) {

$myFile = "/mounted-storage/home112c/sub002/sc49404-CVYU/avm/log_file.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
fwrite($fh, "getTarget called\n");
fclose($fh);	

    return FALSE;
  }

  function getOriginalThumbnailPath() {

    $parts = $this->get_parameters();	

	  $handler_to_vt = new MediaInternetVideoToolHandler($this->interpolateUrl());
		$orig_vt_image_url = ''; 
    if($info = $handler_to_vt->getOEmbed()) {
			$orig_vt_image_url = $info['thumbnail_url'];			
		}

$myFile = "/mounted-storage/home112c/sub002/sc49404-CVYU/avm/log_file.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
fwrite($fh, "getOriginalThumbnailPath called\n");
fwrite($fh, "getOriginalThumbnailPath http://imgs.videotool.dk/23/".check_plain($parts['vn'])."_large.jpg\n");
fwrite($fh, "getOriginalThumbnailPath url from interpolateUrl ".$this->interpolateUrl()."\n");
fwrite($fh, "getOriginalThumbnailPath from oembed ".$orig_vt_image_url."\n");
fclose($fh);		
	
    return $orig_vt_image_url;
  }

  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $local_path = file_default_scheme() . '://media-videotool/' . check_plain($parts['vn']) . '.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      $response = drupal_http_request($this->getOriginalThumbnailPath());
      if (!isset($response->error)) {
        file_unmanaged_save_data($response->data, $local_path, TRUE);
      }
      else {
        @copy($this->getOriginalThumbnailPath(), $local_path);
      }
    }

$myFile = "/mounted-storage/home112c/sub002/sc49404-CVYU/avm/log_file.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
fwrite($fh, "getLocalThumbnailPath called\n");
fwrite($fh, "getLocalThumbnailPath local path: ".$local_path."\n");
fclose($fh);	
		
    return $local_path;
  }
}
