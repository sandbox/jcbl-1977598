<?php

/**
 * @file media_videotool/includes/MediaVideoToolBrowser.inc
 *
 * Definition of MediaVideoToolBrowser.
 */

/**
 * Media browser plugin for displaying a specific view and display.
 */
class MediaVideoToolBrowser extends MediaBrowserPlugin {
  /**
   * Implements MediaBrowserPluginInterface::access().
   */
  public function access($account = NULL) {

$myFile = "/mounted-storage/home112c/sub002/sc49404-CVYU/avm/log_file.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
fwrite($fh, "access called\n");
fclose($fh);	

    // @TODO: media_access() is a wrapper for file_entity_access(). Switch to the
    // new function when Media 1.x is deprecated.
    return media_access('create', $account);
  }

  /**
   * Implements MediaBrowserPlugin::view().
   */
  public function view() {

$myFile = "/mounted-storage/home112c/sub002/sc49404-CVYU/avm/log_file.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
fwrite($fh, "view called\n");
fclose($fh);	
	
    // Check if the user is able to add remote media.
    if (user_access('add media from remote sources')) {
      $build['form'] = drupal_get_form('media_videotool_add', $this->params['types'], $this->params['multiselect']);
      return $build;
    }
  }
}
