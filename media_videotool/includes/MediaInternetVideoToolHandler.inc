<?php

/**
 * @file media_videotool/includes/MediaInterenetVideoToolHandler.inc
 *
 * Contains MediaInternetVideoToolHandler.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetVideoToolHandler extends MediaInternetBaseHandler {
  /**
   * Check if a VideoTool video id is valid.
   *
   * Check against the oembed stream instead of the gdata api site to
   * avoid "yt:quota too_many_recent_calls" errors.
   *
   * @return
   *   Boolean.
   */
  static public function validId($id) {
    $url = 'http://www.videotool.dk/oembed?url=http%3A//media.videotool.dk/%3Fvn%3D'. $id;

$myFile = "/mounted-storage/home112c/sub002/sc49404-CVYU/avm/log_file.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
fwrite($fh, "validId called\n");
fwrite($fh, "validId test url: ".$url."\n");
fclose($fh);		
		
    $response = drupal_http_request($url, array('method' => 'HEAD'));
    if ($response->code != 200) {
      throw new MediaInternetValidationException("The VideoTool video ID is invalid or the video was deleted.");
    }
    return TRUE;
  }

  public function parse($embedCode) {

$myFile = "/mounted-storage/home112c/sub002/sc49404-CVYU/avm/log_file.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
fwrite($fh, "parse called\n");
fclose($fh);	
	
    $patterns = array(
      '@videotool\.dk/[#\?].*?vn=([^"\& ]+)@i',
    );
		

    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
			print('<div style="background-color: #00ff00;">');
			print_r('Matches: '.$matches);
			print('</div>');
      // @TODO: Parse is called often. Refactor so that valid ID is checked
      // when a video is added, but not every time the embedCode is parsed.
      if (isset($matches[1]) && self::validId($matches[1])) {
        return file_stream_wrapper_uri_normalize('videotool://vn/' . $matches[1]);
      }
    }
  }

  public function claim($embedCode) {

$myFile = "/mounted-storage/home112c/sub002/sc49404-CVYU/avm/log_file.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
fwrite($fh, "claim called\n");
fclose($fh);	

    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

$myFile = "/mounted-storage/home112c/sub002/sc49404-CVYU/avm/log_file.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
fwrite($fh, "getFileObject called\n");
fwrite($fh, "getFileObject uri: ".$uri."\n");
fwrite($fh, "getFileObject file: ".print_r($file, true)."\n");
fclose($fh);					
		
    if (empty($file->fid) && $info = $this->getOEmbed()) {
      $file->filename = truncate_utf8($info['title'], 255);
    }

    return $file;
  }

  /**
   * Returns information about the media. See http://video.search.yahoo.com/mrss.
   *
   * @return
   *   If ATOM+MRSS information is available, a SimpleXML element containing
   *   ATOM and MRSS elements, as per those respective specifications.
   *
   * @todo Would be better for the return value to be an array rather than a
   *   SimpleXML element, but media_retrieve_xml() needs to be upgraded to
   *   handle namespaces first.
	 * THIS FUNCTION IS UNCOMMENTED ##############################################################################################################################	 
  public function getMRSS() {

$myFile = "/mounted-storage/home112c/sub002/sc49404-CVYU/avm/log_file.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
fwrite($fh, "getMRSS called\n");
fclose($fh);	

    $uri = $this->parse($this->embedCode);
    $video_id = arg(1, file_uri_target($uri));
    $rss_url = url('http://gdata.videotool.com/feeds/api/videos/' . $video_id, array('query' => array('v' => '2')));
    // @todo Use media_retrieve_xml() once it's upgraded to include elements
    //   from all namespaces, not just the document default namespace.
    $request = drupal_http_request($rss_url);
    if (!isset($request->error)) {
      $entry = simplexml_load_string($request->data);
    }
    else {
      throw new Exception("Error Processing Request. (Error: {$response->code}, {$response->error})");

      //if request wasn't successful, create object for return to avoid errors
      $entry = new SimpleXMLElement();
    }
    return $entry;
  }
   */
	
  /**
   * Returns information about the media. See http://www.oembed.com/.
   *
   * @return
   *   If oEmbed information is available, an array containing 'title', 'type',
   *   'url', and other information as specified by the oEmbed standard.
   *   Otherwise, NULL.
   */
  public function getOEmbed() {
    $uri = $this->parse($this->embedCode);
    $external_url = file_create_url($uri);
		
$myFile = "/mounted-storage/home112c/sub002/sc49404-CVYU/avm/log_file.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
fwrite($fh, 'getOEmbed called: '.$external_url."\n");
fclose($fh);		
		
    $oembed_url = url('http://www.videotool.dk/oembed', array('query' => array('url' => $external_url, 'format' => 'json')));
    $response = drupal_http_request($oembed_url);
    if (!isset($response->error)) {
      return drupal_json_decode($response->data);
    }
    else {
      throw new Exception("Error Processing Request. (Error: {$response->code}, {$response->error})");
      return;
    }
  }
}
